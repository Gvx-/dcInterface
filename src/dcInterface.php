<?php
/* -- BEGIN LICENSE BLOCK -----------------------------------------------------
 * This file is part of dcInteface for Dotclear 2.
 * Copyright © 2008-2015 Gvx
 * Licensed under the GPL version 2.0 license.
 * (http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * -- END LICENSE BLOCK -----------------------------------------------------*/

if (!defined('DC_RC_PATH')) { return; }

if (!defined('NL')) { define('NL',"\n"); }			# New Line

abstract class dcInterface_0135 {
	# -- INFORMATIONS BLOCK BEGIN ----------------------------------------------------
	const Name      	= 'dcInteface for dotclear version 2.5+';
	const Version   	= '0.14.0-r0135';
	const Author    	= 'Gvx';
	const Copyright 	= '© 2008-2015 Gvx';
	const Support			= 'http://';
	const Licence   	= 'http://www.gnu.org/licenses/old-licenses/gpl-2.0.html';
	# -- INFORMATIONS BLOCK END ------------------------------------------------------

	protected  $pluginId;

	private $urlSupport;
	private $logfile;
	
	protected static $adminJs = '';
	protected static $publicJs = '';

	# -- PUBLIC INTERFACE ------------------------------------------------------------
	public function __construct($support='', $root='') {
		global $core;
		if(empty($root)) { $root = dirname(dirname(__FILE__)); }
		if(!is_file($root.'/_define.php')) { throw new DomainException(__('Invalid plugin directory')); }
		$this->pluginId = basename($root);
		//$this->log('[function] => dcInterface::__construct');		// debug
		$this->urlSupport = $support;
		$this->setDefaultConfigs();
		$behavior = $core->getBehaviors('adminPageFooter');
		if($behavior === null || !in_array(array($this,'jsCode'), $behavior)) {
			$core->addBehavior('adminPageFooter', array(__CLASS__,'jsCode'));
		}
		$behavior = $core->getBehaviors('publicFooterContent');
		if($behavior === null || !in_array(array($this,'jsCode'), $behavior)) {
			$core->addBehavior('publicFooterContent', array(__CLASS__,'jsCode'));
		}
	}

	final public function p_url($args='', $escape=false) {
		return self::urlBuild('plugin.php?p='.$this->pluginId, $args, $escape);
	}

	final public function pf_url($file, $self=true) {
		return 'index.php?pf='.($self ? $this->pluginId.'/' : '').trim($file,'/');
	}

	final public function info($info=null) {
		global $core;
		if(empty($info) || $info == 'id') {
			return $this->pluginId;
		} elseif($info == 'class') {
			return get_class();
		} elseif($info == 'is_deletable') {
			$p_paths = explode(PATH_SEPARATOR, DC_PLUGINS_ROOT);
			$p_path = array_pop($p_paths);
			unset($p_paths);
			return is_dir($p_path) && is_writeable($p_path) && preg_match('!^'.preg_quote($p_path,'!').'!', $this->info('root'));
		} else {
			return $core->plugins->moduleInfo($this->pluginId,$info);
		}
	}

	final public function settings($key, $value=null, $global=false) {
		global $core;
		$pluginId = $this->pluginId;
		if(is_null($value)) {
			return $core->blog->settings->$pluginId->$key;
		} else {
			$core->blog->settings->$pluginId->put($key, $value, null, null, true, $global);
		}
	}

	final public function userSettings($key, $value=null, $global=false) {
		global $core;
		$pluginId = $this->pluginId;
		if(is_null($value)) {
			return $core->auth->user_prefs->$pluginId->$key;
		} else {
			$core->auth->user_prefs->$pluginId->put($key,$value, null, null, true, $global);
		}
	}

	final public function adminDashboard() {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core;
		$callback = array($this,'adminDashboardFavs');
		if(is_callable($callback)) { $core->addBehavior('adminDashboardFavs',$callback); }
	}

	final public function adminDashboardFavs($core, $favs) {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		$favs[$this->pluginId] = new ArrayObject(array(
			$this->pluginId,
			$this->info('name'),
			$this->p_url(),
			$this->pf_url('inc/admin/icon.png'),
			$this->pf_url('inc/admin/icon-big.png'),
			$this->info('permissions'),
			null,
			null
		));
	}

	final public function adminMenu($icon='inc/admin/icon.png', $itemMenu='Plugins', $permissions='admin') {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core, $_menu;
		if(empty($icon)) { $icon = 'inc/admin/icon.png'; }
		if(empty($itemMenu)) { $itemMenu='Plugins'; }
		if(empty($permissions)) { $permissions='admin'; }
		$_menu[$itemMenu]->addItem(
			html::escapeHTML(__($this->info('name'))),																	// Item menu
			$this->p_url(),																															// Page admin url
			$this->pf_url($icon),																												// Icon menu
			preg_match('/'.$this->p_url().'(&.*)?$/',$_SERVER['REQUEST_URI']),					// Pattern url
			$core->auth->check($permissions,$core->blog->id)														// Permissions minimum
		);
	}

	final public function adminFormValid() {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core;
		if (isset($_POST['save'])) {
			$this->settings('enabled',!empty($_POST['enabled']));
			if(isset($_POST[$id.'notepad'])) { $this->settings('notepad', base64_encode(trim($_POST[$id.'notepad']))); }
			if($this->adminSaveConfig()) {
				$core->blog->triggerBlog();
				http::redirect($this->p_url($this->messageNotification('saveOK')));
			} else {
				$core->error->add(__('Unable to save the configuration'));
			}
		}
	}

	final public function install($dcMinVer=null) {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core;
		try {
			# Check DC version
			if(!empty($dcMinVer) && is_string($dcMinVer)) {
				if (version_compare(DC_VERSION,$dcMinVer,'<')) {
					$core->plugins->deactivateModule($this->pluginId);
					throw new Exception(sprintf(__('%s require %s %s. %s was quenched.'),$this->info('name'),'Dotclear',$dcMinVer,$this->info('name')));
				}
			}
			$res = true;
			# On lit la version du plugin - le nom passé à moduleInfo est le nom du répertoire où se trouve votre plugin !
			$m_version = $core->plugins->moduleInfo($this->pluginId,'version');
			# On lit la version du plugin dans la table des versions
			$i_version = $core->getVersion($this->info('name'));
			# La version dans la table est supérieure ou égale à celle du module, on ne fait rien puisque celui-ci est installé
			if (version_compare($i_version,$m_version,'>=')) { return; }
			# creation des settings et autres actions d'installation
			$this->setDefaultConfigs();
			# --BEHAVIOR-- pluginCustomInstall
			$res = $core->callBehavior('pluginCustomInstall', $core, $this->pluginId);
			if(is_null($res)) { $res = true; }
			$core->blog->triggerBlog();
			# on enregistre la version du plugin
			$core->setVersion($this->info('name'),$m_version);
			return $res;
		} catch (Exception $e) {
			$core->error->add($e->getMessage());
			return false;
		}
	}

	final public function uninstall($entire=false) {
		global $core;
		if(empty($this->$pluginId)) { return; }
		try {
			if ($this->info('is_deletable')) {
				if($entire) {
					# --BEHAVIOR-- pluginCustomUninstall
					$core->callBehavior('pluginCustomUninstall', $core, $this->pluginId);
					# suppression des settings
					$this->deleteConfigs();
					# suppression de la version du plugin
					$core->delVersion($this->info('name'));
				}
				$plugin = $core->plugins->getModules($this->pluginId);
				$plugin['id'] = $this->pluginId;
				# --BEHAVIOR-- moduleBeforeDelete
				$core->callBehavior('pluginBeforeDelete', $plugin);
				# suppression du repertoire du plugin
				$core->plugins->deleteModule($this->pluginId);
				# --BEHAVIOR-- moduleAfterDelete
				$core->callBehavior('pluginAfterDelete', $plugin);
				$core->blog->triggerBlog();
				if(method_exists('dcPage','addSuccessNotice')) { dcPage::addSuccessNotice($this->info('name').' '.__('success uninstall')); }
				http::redirect('index.php');
			} else {
				throw new Exception(__('You don\'t have permissions to delete this plugin.'));
			}
		} catch (Exception $e) {
			$core->error->add($e->getMessage());
			http::redirect('index.php');
		}
	}

	final public static function adminBlogPrefs() {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core;
		$callback_form = array($this,'adminFormBlogPrefs');
		$callback_save = array($this,'adminSaveBlogPrefs');
		if(is_callable($callback_form) && is_callable($callback_save)) {
			$core->addBehavior('adminBlogPreferencesForm',$callback_form);
			$core->addBehavior('adminBeforeBlogSettingsUpdate',$callback_save);
		}
	}

	final public function adminPage($page=null) {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core;
		if(empty($page)) { $page = empty($_REQUEST['popup']) ? (empty($_REQUEST['dcPage']) ? 'default' : $_REQUEST['dcPage']) : 'popup'; }
		$urls = array_merge(
			array(
				'default'		=> dirname(__FILE__).'/admin/admin.php',
				'popup'			=> dirname(__FILE__).'/admin/popup.php',
				'uninstall'	=> dirname(__FILE__).'/_uninstall.php'
			),
			$this->adminRegisterUrls()
		);
		if(empty($page) || !array_key_exists($page,$urls)) { $page = 'default'; }
		if(is_file($urls[$page])) { require_once $urls[$page]; } else { /* TODO: page 404 */}
	}

	final public function adminBaseline($items=array()) {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core;
		if(version_compare(DC_VERSION,'2.6','<')) {
			if(empty($items)) { $items = array( $this->info('name') => ''); }
			$html = '<span class="page-home">'.html::escapeHTML($core->blog->name).'</span>';
			foreach($items as $item => $url) {
				$html .= ' &rsaquo; '.(empty($url) ? '<span class="page-title">'.html::escapeHTML($item).'</span>' : '<a href="'.$url.'">'.html::escapeHTML($item).'</a>');
			}
			echo '<h2>'.$html.'</h2>'.$this->adminMessage().NL;
		} else {
			echo dcPage::breadcrumb(array_merge(array(html::escapeHTML($core->blog->name) => ''),$items)).dcPage::notices().NL;
		}
		echo '<div id="no-js" class="error"><p>'.__('Javascript is disabled').'</p></div>';		// warning JS fail
		echo '<div id="error-js" class="error"><p>'.__('A Javascript error occurred:').'</p></div>';		// warning JS fail
	}

	final public function adminFooterInfo($echo=true) {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		$html  =
		'<p class="right">
			<img style="vertical-align: middle;" src="'.$this->pf_url('inc/admin/icon.png').'" alt="'.__('icon plugin').'"/>&nbsp;&nbsp;'.
			html::escapeHTML($this->info('name')).'&nbsp;'.
			__('Version').'&nbsp;:&nbsp;'.html::escapeHTML($this->info('version')).'&nbsp;-&nbsp;'.
			__('Author(s)').'&nbsp;:&nbsp;'.html::escapeHTML($this->info('author')).
			(!empty($this->urlSupport) ? '&nbsp;-&nbsp;<a href="'.$this->urlSupport.'">'.__('Support').'</a>' : '').'
		</p>'.NL;
		if($echo) { echo $html; }
		return $html;
	}

	protected function adminFormBlogPrefs($core) {
		$name = __($this->info('name'));
		echo	'<div class="fieldset">
						<h3>'.html::escapeHTML($name).'</h3>
						<p><label class="classic" for="'.$this->pluginId.'_enabled">
							'.form::checkbox($this->pluginId.'_enabled','1',$this->settings('enabled')).html::escapeHTML(sprintf(__('Enable %s on this blog'),$name)).'
						</label></p>
						'.$this->adminFooterInfo(false).'
					</div>';
	}

	protected function adminSaveBlogPrefs() {
		$this->settings('enabled',!empty($_POST[$this->pluginId.'_enabled']));
		return true;
	}

	# -- INTERNAL INTERFACE ----------------------------------------------------------
	protected function getAdminMessages() {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		/* type:
			"success"
			"warning"
			"error"
			"message"
			"static"
		*/
		//$this->log('[function] => dcInterface::getAdminMessages');		// debug
		return array(
			'saveOK'			=> array('msg' => __('Configuration successfully updated.'), 'type' => 'success'),
			'updateOK'		=> array('msg' => __('Code successfully updated.'), 'type' => 'success')
		);
	}

	final protected function messageNotification($index) {
		if(version_compare(DC_VERSION,'2.7','<')) { return 'msg='.$index; }
		$messages = $this->getAdminMessages();
		if(array_key_exists($index, $messages)) {
			dcPage::addNotice($messages[$index]['type'], $messages[$index]['msg']);
		}
		return '';
	}

	final private function adminMessage($msg='', $type='message') {
		$html = '';
		if(empty($msg) && !empty($_GET['msg'])) {
			$index = $_GET['msg'];
			$messages = $this->getAdminMessages();
			if(array_key_exists($index, $messages)) {
				$msg = $messages[$index]['msg'];
				$type = $messages[$index]['type'];
			}
		}
		if(!empty($msg)) {
			if (version_compare(DC_VERSION,'2.6','<')) {
				$html = '<p class="message">'.$msg.'</p>';
			} elseif($type == 'success') {
				$html = dcPage::success($msg,true,false,false);
			} elseif($type == 'warning') {
				$html = dcPage::warning($msg,true,false,false);
			} else {
				$html = dcPage::message($msg,true,false,false);
			}
		}
		return $html;
	}

	final protected static function urlBuild($url, $args='', $escape=true) {
		if(is_array($args)) { $args = http_build_query($args); }
		$args = empty($args) ? '' : (strpos($url,'?') === false ? '?' : '&').trim($args,'?&');
		return $escape ? html::escapeHTML($url.$args) : $url.$args;
	}
	
	final public static function jsCode() {
		$js ='';
		if(defined('DC_CONTEXT_ADMIN')) {
			if(!empty(self::$adminJs)) {
				$js = trim(
					'// -- check javascript enabled and free bug --------------------
					$("body").addClass("with-js").removeClass("no-js").find("#no-js, #error-js").hide();
					window.onerror = function (msg, url, line) {
						$("#error-js").append(
							"<ul><li><strong>Message:</strong> " + msg + "</li><li><strong>URL:</strong> " + url + "</li><li><strong>LINE:</strong> " + line + "</li></ul>"
						).show();
					};'
				).NL.trim(
					'// -- button submit and reset ----------------------------------
					var $cmd = $(\'input[type="submit"], input[type="reset"]\');
					$cmd.attr("disabled", "disabled");
					$("body").on("reset", function (e) {
						$cmd.attr("disabled", "disabled");
					});'
				).self::$adminJs;
			}
		} else {
			$js = trim(self::$publicJs);
		}
		if(empty($js)) { return; }
		echo trim('
			<script type="text/javascript">
				//<![CDATA[
					(function($, window, document, undefined) {
						$(document).ready(function () {'.NL.$js.'
						});
					} (jQuery, window, document));
				//]]>
			</script>'
		).NL;
	}
	
	final protected static function adminJsCode($code) {
		self::$adminJs .= NL.trim($code);
	}

	final protected static function publicJsCode($code) {
		self::$publicJs .= NL.trim($code);
	}

	# -- ADMIN GUI -------------------------------------------------------------------
	final protected function blockHeader($title, $ctrls='toggle+full', $options=array()) {
		$options = array_merge(
			array(
				'tag'				=> 'h3',
				'bt_toggle'	=> '▼',
				'bt_full'		=> '▲'
			), (is_array($options) ? $options : array())
		);
		$title = (empty($title) ? '' : '<'.$options['tag'].'>'.$title.'</'.$options['tag'].'>');
		$buttons = '';
		if(strpos($ctrls, 'toggle') !== false) { $buttons .= '<a class="toggle" title="'.__('Open/close box').'" href="#">'.$options['bt_toggle'].'</a>'; }
		if(strpos($ctrls, 'full') !== false) { $buttons .= '<a class="full" title="'.__('Fullscreen').'" href="#">'.$options['bt_full'].'</a>'; }
		self::adminJsCode('// -- blockHeader ----------------------------------------------
			$(".block-header .block-ctrl .toggle").on("click", function (e) {
				$(this).parent().parent().siblings().toggle().focus().parent().toggleClass("full-window", false);
				return false;
			});
			$(".block-header .block-ctrl .full").on("click", function (e) {
				$(this).parent().parent().siblings().toggle(true).focus().parent().toggleClass("full-window");
				return false;
			});
		');
		return '<div class="block-header"><p class="block-ctrl">'.$buttons.'</p>'.$title.'</div>'.NL;
	}

	final protected function notepad() {
		$html =
			'<div class="fieldset notepad">
				'.$this->blockHeader(__('Annotations'), 'toggle+full').
				form::textarea('notepad', 80, 6, html::escapeHTML(base64_decode($this->settings('notepad'))), 'maximal clear').'
			</div>'.NL;
		self::adminJsCode('// -- notepad --------------------------------------------------
			$(".notepad .block-header .block-ctrl .toggle").trigger("click");
			$("#notepad").on("keypress", function (e) {
				$(\'#save, input[type="reset"]\').removeAttr("disabled");
			});
		');
		return $html;
	}

	final protected function buttonsBar($buttons='save+reset', $defButtons=array(), $enclose='<p class="button-bar clear">%s</p>') {
		$defs = array_merge($this->getButtons(), is_array($defButtons) ? $defButtons : array());
		$buttons = explode('+', str_replace(array(' ',',',';',':','|'), '+', $buttons));
		$html = '';
		$left = '';
		$center = '';
		$right = '';
		if(!empty($buttons)) {
			foreach($buttons as $button) {
				if(array_key_exists($button, $defs) && (isset($defs[$button]['if']) ? $defs[$button]['if'] : true)) {
					$attr = trim(
						' '.(empty($defs[$button]['id']) ? '' : 'id="'.$defs[$button]['id'].'"').
						' '.(empty($defs[$button]['title']) ? '' : 'title="'.$defs[$button]['title'].'"').
						' class="'.trim(
							' '.(empty($defs[$button]['type']) ? '' : trim($defs[$button]['type'])).
							' '.(empty($defs[$button]['class']) ? '' : trim($defs[$button]['class']))
						).'"'.
						' '.(empty($defs[$button]['attr']) ? '' : $defs[$button]['attr'])
					);
					if($defs[$button]['type'] == 'input-submit') {
						$element = '<input type="submit" '.$attr.' name="'.$defs[$button]['id'].'" value="'.$defs[$button]['value'].'" />';
					} elseif($defs[$button]['type'] == 'input-reset') {
						$element = '<input type="reset" '.$attr.' value="'.$defs[$button]['value'].'" />';
					} elseif($defs[$button]['type'] == 'input-button') {
						$element = '<input type="button" '.$attr.' name="'.$defs[$button]['id'].'" value="'.$defs[$button]['value'].'" />';
					} elseif($defs[$button]['type'] == 'separator') {
						$element = '<span '.$attr.'>'.$defs[$button]['value'].'</span>';
					} else {
						$element = '<a '.$attr.' href="'.(empty($defs[$button]['href']) ? '#' : $defs[$button]['href']).'">'.$defs[$button]['value'].'</a>';
					}
					if(empty($defs[$button]['position'])) { $defs[$button]['position'] = 'left'; }
					if($defs[$button]['position'] == 'right') {
						$right .= $element.NL;
					} elseif($defs[$button]['position'] == 'center') {
						$center .= $element.NL;
					} else {
						$left .= $element.NL;
					}
					if(!empty($defs[$button]['js'])) { self::adminJsCode($defs[$button]['js']); }
				}
			}
			$html = sprintf(
				$enclose, NL.
				(empty($left) ? '' : '<span class="bb-left">'.NL.$left.'</span>'.NL).
				(empty($center) ? '&nbsp;' :'<span class="bb-center">'.NL.$center.'</span>').NL.
				(empty($right) ? '' : '<span class="bb-right">'.NL.$right.'</span>'.NL)
			);
		}
		return $html;
	}

	public function activate($more='') {
		if (!defined('DC_CONTEXT_ADMIN')) { return; }
		global $core;
		$html =
			'<form action="'.html::escapeHTML($this->p_url()).'" method="post" id="'.html::escapeHTML($this->pluginId).'-form-tab-1">
				<p>'.$core->formNonce().'</p>
				<div class="fieldset">
					<h3>'.__('Activation').'</h3>
					<p>
						'.form::checkbox('enabled','1',$this->settings('enabled'))
						.'<label class="classic" for="enabled">'.sprintf(__('Enable %s on this blog'),html::escapeHTML(__($this->info('name')))).'</label>
					</p>
				</div>
				'.(empty($more) ? '' : $more).'
				'.$this->notepad().$this->buttonsBar('save+reset+uninstall').'
			</form>';
		return $html;
	}

	public static function cssLoad($file, $media='', $echo=true) {
		$html = '<link rel="stylesheet" type="text/css" href="'.$file.'"'.(!empty($media) ? ' media="'.$media.'"' : '').' />'.NL;
		if($echo) { echo $html; }
		return $html;
	}

	# -- DEBUG -----------------------------------------------------------------------
	final public function log($text, $value=null) {
		if(empty($this->logfile)) {				# initialization
			$logDir = DC_TPL_CACHE.'/logs';
			if(!is_dir($logDir)) { mkdir($logDir, 0755, true); }
			$this->logfile = $logDir.'/log_'.$this->pluginId.'.txt';
			if(is_file($this->logfile)) { @unlink($this->logfile); }
		}
		if(!empty($text)) {
			if(!empty($value)) { $text .= ':'.NL.print_r($value,true).NL.str_pad('', 60, '*'); }
			file_put_contents ($this->logfile ,'['.date('Y-m-d-H-i-s').'] : '.$text.NL,FILE_APPEND);
		}
	}

	# -- EXEMPLE: INTERFACE CLASS DCINTERFACE ----------------------------------------

	protected function setDefaultConfigs() {
		global $core;
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		# config plugin
		$pluginId = $this->pluginId;
		$core->blog->settings->addNamespace($pluginId);
		$core->blog->settings->$pluginId->put('enabled',false,'boolean',__('Enable plugin'),false,true);
		$core->blog->settings->$pluginId->put('notepad',base64_encode(''),'string',__('notepad'),false,true);
		# user config plugin
		//$core->auth->user_prefs->addWorkSpace($pluginId);
		//$core->auth->user_prefs->$pluginId->put('enabled',false,'boolean',__('Enable plugin'),false,true);
	}

	protected function deleteConfigs() {
		global $core;
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		# delete config plugin
		$pluginId = $this->pluginId;
		$core->blog->settings->$pluginId->drop('enabled');
		$core->blog->settings->$pluginId->drop('notepad');
		# delete user config plugin
		//$core->auth->user_prefs->$pluginId->drop('enabled');
	}

	protected function adminRegisterUrls() {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		return array(
			'popup' 		=> dirname(__FILE__).'/admin/popup.php',
			'default'		=> dirname(__FILE__).'/admin/admin.php'
		);
		return array();
	}
	
	protected function getButtons() {
		global $core;
		return array(
			'save'			=> array(
				'type'			=> 'input-submit',
				'id'				=> 'save',
				'value'			=> __('Save'),
				'title'			=> __('Save the configuration')
			),
			'reset'			=> array(
				'type'			=> 'input-reset',
				'value'			=> __('Cancel'),
				'title'			=> __('Undo changes')
			),
			'uninstall'	=> array(
				'type'			=> 'button',
				'id'				=> 'uninstall',
				'value'			=> __('Delete'),
				'title'			=> __('Plugin uninstall'),
				'class'			=> 'delete',
				'position'	=> 'right',
				'href'			=> $this->p_url('dcPage=uninstall'),
				'if'				=> ($core->auth->isSuperAdmin() && $this->info('is_deletable') && !$this->settings('enabled')),
				'js'				=> '$("#uninstall").on("click", function(e) { return window.confirm("'.sprintf(__('Confirm removing the %s plugin'),html::escapeHTML($this->info('name'))).'"); });'
			),
			/*
			'button1'		=> array(
				'type'			=> 'input-button',
				'id'				=> 'id',
				'value'			=> __('value'),
				'title'			=> __('title')
			),
			'button2'		=> array(
				'type'			=> 'button',
				'id'				=> 'id',
				'href'			=> '#',
				'value'			=> __('value'),
				'title'			=> __('title')
				),
			'button3'		=> array(
				'type'			=> 'icon',
				'id'				=> 'id',
				'href'			=> '#',
				'value'			=> '<img src="" alt="" />',
				'title'			=> __('title')
			),
			*/
			'separator'	=> array(
				'type'			=> 'separator'
			)
		);
	}

	protected function adminSaveConfig() {
		if(!defined('DC_CONTEXT_ADMIN')) { return; }
		//$this->log('class=dcInterface::$_POST',$_POST);			// debug
		# Saving configurations
		return true;
	}

}
